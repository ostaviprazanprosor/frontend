var app = angular.module('safetyApp');

app.controller('ProfilCtrl', ['$scope','$window', 'HttpFactory', function($scope,$window,httpFactory) {
    $scope.contacts = [];
    $scope.searchRes = [];

    httpFactory.getContacts($window.localStorage.id).then(function(success) {
        console.log(success);
        $scope.contacts = success.data;
    }, function (err) {
        console.log(err);
    });

    $scope.searchContact = function(req) {
        console.log(req);
        httpFactory.searchContact(req).then(function(success) {
            console.log(success);
            $scope.searchRes = success.data;
        },function(err) {
            console.log(err);
        })
    }

    $scope.addContact = function(id) {
        console.log(id);
        creds = {
            forId: $window.localStorage.id,
            toId: id
        }

        httpFactory.addContact(creds).then(function(success) {
            console.log(success);
        },function(err) {
            console.log(err);
        })

    }

}])