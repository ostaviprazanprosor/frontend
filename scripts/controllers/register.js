var app = angular.module('safetyApp');

app.controller('RegisterCtrl', ['$scope','$window', '$location','HttpFactory', function ($scope,$window,$location,httpFactory) {
    var regVm = this;
    
    regVm.register = function (creds) {
        httpFactory.register(creds).then(function(success) {
            console.log(success);
            $scope.go();
        }, function(err) {
            console.log(err);
            delete $window.localStorage.token;
        });
    };

    $scope.go = function() {
        $location.path('/main');
    }
}])
