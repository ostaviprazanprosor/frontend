var app = angular.module('safetyApp');

app.controller('MainCtrl', ['$scope', 'geolocation', '$timeout', '$rootScope', '$interval', '$window', 'HttpFactory', 'notify', 'notifFactory', function ($scope, geolocation, $timeout, $rootScope, $interval, $window, httpFactory, notify,notifFactory) {
    var mainVm = this;
    var lat;
    var long;
    var map;
    var googMap;
    var geocoder;

    var dirDisplay;
    var dirService;

    var routeMarkers = [];

    $scope.notifications = [];
    mainVm.oldNotifications = [];


    mainVm.duration;
    mainVm.formatedDuration;

    geolocation.getLocation().then(function (data) {
        lat = data.coords.latitude;
        long = data.coords.longitude;

        $scope.marker = {
            id: 0,
            coords: {
                latitude: lat,
                longitude: long
            },
            options: {draggable: false},
            events: {}
        };

        initMap();
    })

    mainVm.test = 'radi';

    var markers = [];

    var map;

    function initMap() {
        map = new google.maps.Map(document.getElementById('map_canvas'), {
            center: {lat: lat, lng: long},
            zoom: 16
        });

        geocoder = new google.maps.Geocoder;
        dirDisplay = new google.maps.DirectionsRenderer;
        dirService = new google.maps.DirectionsService;

        getStartAddress(lat, long);

        marker = new google.maps.Marker({
            position: {
                lat: lat,
                lng: long
            },
            map: map,
            title: 'Tvoja Lokacija'
        });
        markers.push(marker);

        dirDisplay.setMap(map);
        map.addListener('click', function (e) {
            markers[0].setMap(null);
            calcRoute(e.latLng.lat(), e.latLng.lng());
        })

        var input = document.getElementById('search');
        var searchBox = new google.maps.places.SearchBox(input);

        searchBox.addListener('places_changed', function () {
            var places = searchBox.getPlaces();

            if (places.length == 0) {
                return;
            }

            markers.forEach(function (marker) {
                marker.setMap(null);
            });
            markers = [];

            var bounds = new google.maps.LatLngBounds();
            places.forEach(function (place) {
                var icon = {
                    url: place.icon,
                    size: new google.maps.Size(71, 71),
                    origin: new google.maps.Point(0, 0),
                    anchor: new google.maps.Point(17, 34),
                    scaledSize: new google.maps.Size(25, 25)
                };

                markers.push(new google.maps.Marker({
                    map: map,
                    icon: icon,
                    title: place.name,
                    position: place.geometry.location
                }));

                if (place.geometry.viewport) {
                    bounds.union(place.geometry.viewport);
                } else {
                    bounds.extend(place.geometry.location);
                }
            });
            map.fitBounds(bounds);
        });
    };

    function getStartAddress(latitude, longitude) {
        var latlng = new google.maps.LatLng(latitude, longitude);

        geocoder.geocode({
            location: latlng
        }, function (data, status) {
            mainVm.startAddress = data[0].formatted_address;
        })
    }

    function getEndAddress(latitude, longitude) {
        var latlng = new google.maps.LatLng(latitude, longitude);

        geocoder.geocode({
            location: latlng
        }, function (data, status) {
            mainVm.endAddress = data[0].formatted_address;
        })
    }

    function calcRoute(endLat, endLng) {
        var start = new google.maps.LatLng(lat, long);
        var end = new google.maps.LatLng(endLat, endLng);
        var request = {
            origin: start,
            destination: end,
            travelMode: google.maps.TravelMode.WALKING
        };
        dirService.route(request, function (response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                dirDisplay.setDirections(response);
                var route = response.routes[0];
                mainVm.duration = route.legs[0].duration.value;
                mainVm.formatedDuration = new Date().getTime() + mainVm.duration * 1000;

                getEndAddress(endLat, endLng);

                $scope.$apply();
            }
        });
    }

    $scope.pollServer = function () {
        var interval = $interval(function () {
            if (!$window.localStorage.token) {
                $interval.cancel(interval);
                return;
            }

            httpFactory.pollServer($window.localStorage.id).then(function (data) {
                $scope.notifications = data.data;
                mainVm.checkNotifications();
            }, function (err) {
                console.log(err);
            })
        }, 10000);
    }

    $scope.pollServer();

    mainVm.checkNotifications = function () {
        $scope.notifications.forEach(function (notif) {
            var temp = false;
            for(i=0;i<mainVm.oldNotifications.length;i++) {
                 if(notif.time==mainVm.oldNotifications[i].time && notif.friend_in_danger == mainVm.oldNotifications[i].friend_in_danger) {
                    temp = true;
                    break;
                }
            }
            if (!temp) {
                $scope.pop(notif);
                mainVm.oldNotifications.push(notif);
            }
        });
    }
    var timer;
    var notif;

    mainVm.setTimer = function () {
        if (timer) {
            $interval.cancel(timer);
        }

        $scope.secondsPast = 0;
        console.log(mainVm.duration);

        timer = $interval(function () {
            $scope.secondsPast++;
            $scope.secondsLeft--;

            if (mainVm.duration <= $scope.secondsPast) {
                $interval.cancel(timer);
                mainVm.setCurrentNotification();
            }
        }, 1000);

        $scope.secondsLeft = mainVm.duration - $scope.secondsPast;

        notif = notify({
            message: 'Notification',
            templateUrl: 'views/timerNotification.html',
            scope: $scope,
            duration: mainVm.duration * 1000
        });
    }

    var currTimer;

    mainVm.setCurrentNotification = function () {
        $('#notifModal').modal('show');

        currTimer = $timeout(function () {
            httpFactory.dangerSignal($window.localStorage.id);
        }, 300000);
    }

    $scope.cancelTimeout = function () {
        $interval.cancel(timer);
        notif.close();
    }

    $scope.extendTimer = function () {
        console.log('usao');
        $('#extendModal').modal('show');
        console.log($('#extendModal'))
    }

    $scope.call = function () {
        console.log('proslo');
    }


    $scope.pop = function (notif) {
        httpFactory.personDetail(notif.friend_in_danger).then(function(success) {
            notifFactory.addNotification({
                "first_name":success.data.user.first_name,
                "last_name":success.data.user.last_name,
                "email":success.data.user.email,
                "time":notif.time
            })
            console.log(success);
            var isoScope = $scope.$new(true);
            isoScope.firstName = success.data.user.first_name;
            isoScope.lastName = success.data.user.last_name;
            isoScope.date = notif.time;

            notify({
                message: 'Notification',
                templateUrl: 'views/notification.html',
                scope: isoScope,
                position: 'right'
            });
        })
    };

    $scope.sendDangerSignal = function () {
        $interval.cancel(timer);
        httpFactory.dangerSignal($window.localStorage.id).then(function (data) {
            console.log(data);
        });
    }

    $scope.confirmReturn = function () {
        $timeout.cancel(currTimer);
        httpFactory.cancelSignal($window.localStorage.id);
    }

    $scope.mytime = new Date();

    $scope.hstep = 1;
    $scope.mstep = 1;


    $scope.options = {
        hstep: [1, 2, 3],
        mstep: [1, 5, 10, 15, 25, 30]
    };

    $scope.changed = function () {
        console.log('Time changed to: ' + $scope.mytime);
    };

    $scope.clear = function() {
        $scope.mytime = null;
    };

    $scope.updateTime = function () {
        var date = new Date().getTime();
        var mytime = $scope.mytime.getTime();
        mainVm.duration =  Math.round((mytime - date)/1000);

        console.log($scope.mytime.getTime());

        console.log(mainVm.duration);
        mainVm.setTimer();
        $('#extendModal').modal('hide');
    }

    $scope.cancelExtend = function () {
        $('#extendModal').modal('hide');
        httpFactory.cancelSignal($window.localStorage.id);
    }
}]);