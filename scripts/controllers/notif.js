var app = angular.module('safetyApp');

app.controller('NotifCtrl', ['$scope','notify','HttpFactory', function($scope,notify,httpFactory) {
    notifVm = this;

    notifVm.close = function() {
        notify.closeAll();
    }

    notifVm.confirm = function(id) {
        httpFactory.cancelSignal(id);
    }
}])