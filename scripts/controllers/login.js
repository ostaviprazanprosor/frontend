var app = angular.module("safetyApp");

app.controller('LoginCtrl',['$scope', '$window','$location','HttpFactory','$state', function($scope,$window,$location,httpFactory,$state) {
    var loginVm = this;

    loginVm.submitLogin = function (creds) {
        console.log(creds);
        $('#inquiryModal').modal('hide');
        $("html, body").animate({ scrollTop: 0 });

        httpFactory.login(creds).then(function(success) {
            console.log(success);

            $window.localStorage.id = success.data.id;
            $window.localStorage.token = success.data.token;
            $scope.go();
        }, function(err) {
            console.log(err);
        });

        $scope.go = function() {
            $state.go('app');
        }

    }
}]);