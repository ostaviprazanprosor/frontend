var app = angular.module('safetyApp');

app.controller('HeaderCtrl', function ($scope, $location, $window,$state,notify,notifFactory) {
    $scope.showNavbar = function () {
		if($state.current.name!='main') {
            return true;
        } else {
            return false;
        }
    };
    
    $scope.notifications = [];

    $scope.$on('$stateChangeSuccess', function( ){
        if($state.current.name=='main' && $window.localStorage.token) {
            $state.go('app');
        } else if($state.current.name!='main' && !$window.localStorage.token) {
            $state.go('main');
        };
    });

    $scope.go = function (pref) {
        if(pref=='main' && $window.localStorage.token) {
            $state.go('app');
        } else {
            $state.go(pref);
        }
    };

    $scope.logout = function () {
        delete $window.localStorage.id;
        delete $window.localStorage.token;
        notify.closeAll();
        $state.go('main');
    };

    $scope.goToTop = function(){
        $("html, body").animate({ scrollTop: 0 });
    }
    
    $scope.$watch(function () {
        return notifFactory.getNotifications();
    },function(e) {
        console.log(e);
        $scope.notifications = e;
    })

})