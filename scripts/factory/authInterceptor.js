var app = angular.module('safetyApp');

app.factory('authInterceptor', function ($rootScope, $q, $window) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.token) {
                //config.headers.Authorization = 'Token ' + $window.localStorage.token;
            }
            return config;
        },
        response: function (response) {
            if (response.status === 401) {
                response.notAuth = true;
            }

            return response || $q.when(response);
        }
    };
});
