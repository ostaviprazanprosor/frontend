var app = angular.module('safetyApp');

app.factory('HttpFactory',['$http', function($http) {
    var endpoint = 'http://localhost:8000/api/';
    return {
        pollServer: function(id) {
            var request = $http({
                method: "get",
                url: endpoint+'notifications/'+id
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function(err) {
                console.error(err);
            })
            return request;
        },
        dangerSignal: function(id) {
            var request = $http({
                method: "put",
                url: endpoint+'danger/'+id+'/',
                data: $.param({
                    in_danger: true
                }),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }

            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function() {
            })

            return request;
        },
        login: function(creds) {
            var request = $http({
                method: "post",
                url: endpoint+'token-auth/',
                data: {
                    username : creds.username,
                    password : creds.password
                },
                headers: { 'Content-Type': 'application/json' }
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function(err) {
                console.log(err);
            })

            return request;
        },
        register: function(creds) {
            var request = $http({
                method: "post",
                url: endpoint+'register/',
                data: {
                    in_danger: false,
                    user: {
                        username: creds.username,
                        password: creds.password,
                        first_name: creds.firstName,
                        last_name: creds.lastName,
                        email: creds.email
                    }
                },
                headers: { 'Content-Type': 'application/json' }
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function() {
            })

            return request;
        },
        getContacts: function(id) {
            var request = $http({
                method: "get",
                url: endpoint+'contact/'+id+'/'
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function(err) {
                console.log(err);
            })

            return request;
        },
        searchContact: function (req) {
            var request = $http({
                method: "get",
                url: endpoint+'search/?name='+req
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function(err) {
                console.log(err);
            })

            return request;
        },
        addContact: function (creds) {
            var request = $http({
                method: "post",
                url: endpoint+ 'contact/',
                data: $.param({
                    "for_who": creds.forId,
                    "related_contact": creds.toId,
                    "is_approved":'false'
                }),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function(err) {
                console.log(err);
            })

            return request;
        },
        cancelSignal: function(id) {
            var request = $http({
                method: "put",
                url: endpoint+'danger/'+id,
                data: $.param({
                    in_danger: false
                }),
                headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
            });

            request.success(function(data) {
                console.log(data);
            })

            request.error(function() {
            })

            return request;
        },
        pendingApprovals: function(personAsked,personAsking) {
            var request = $http({
                method: "get",
                url: endpoint + 'pendingApprovals/' + personAsked,
                data: $.param({
                    personAsking: personAsking
                }),
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            });

            request.success(function (data) {
                console.log(data);
            })

            request.error(function () {
            })

            return request;
        },
        personDetail: function(id) {
            var request = $http({
                method: "get",
                url: endpoint + 'personDetail/' + id+"/"
            });

            request.success(function (data) {
                console.log(data);
            })

            request.error(function () {
            })

            return request;
        }
    }
}])