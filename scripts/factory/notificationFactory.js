var app = angular.module('safetyApp');

app.factory('notifFactory', [function() {
    var notifications = [];

    return {
        setNotifications: function(arg) {
            notifications = arg;
            console.log('set');
        },
        getNotifications: function() {
            return notifications;
        },
        addNotification: function(arg) {
            for(i=0;i<notifications.length;i++) {
                if(notifications[i].time == arg.time && notifications[i].email == arg.email) {
                    return false;
                }
            }

            notifications.push(arg);
            return true;
        },
        deleteNotification: function(id) {
            notifications.splice(id,1);
        }
    }
}])