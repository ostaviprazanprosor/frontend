var app = angular.module("safetyApp",[
    'ngResource',
    'ui.router',
    'nemLogging',
    'geolocation',
    'cgNotify',
    'ui.bootstrap.timepicker',
    'ui.bootstrap.dropdown'
]);

app.config(['$stateProvider','$urlRouterProvider','$httpProvider', function($stateProvider,$urlRouterProvider,$httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');

    $stateProvider
        .state('main', {
            url:'/main',
            templateUrl: 'views/main.html'
        })
        .state('app', {
            url:'/',
            templateUrl: 'views/app.html',
            controller: 'MainCtrl',
            controllerAs: 'main'
        })
        .state('profil', {
            url:'/profil',
            templateUrl: 'views/profil.html',
            controller: 'ProfilCtrl',
            controllerAs: 'profil'
        })
}]);